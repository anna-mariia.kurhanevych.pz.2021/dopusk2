const autoForm = document.getElementById('auto-form');
let cityInput = document.querySelector('.city-combobox');
let modelSelect = document.querySelector('.model-combobox');
let startRentInput = document.querySelector('.start-rent-input');
let endRentInput = document.querySelector('.end-rent-input');
let firstNameInput = document.querySelector('.first-name-input');
let lastNameInput = document.querySelector('.last-name-input');
let ageInput = document.querySelector('.age-input');
let emailInput = document.querySelector('.email-input');
const cityRegex = /^(?!-)[a-zA-Z-]{2,50}(?<!-)$/;
const firstNameRegex = /^(?!-)[a-zA-Z-]{2,50}(?<!-)$/;
const lastNameRegex = /^(?!-)[a-zA-Z-]{2,50}(?<!-)$/;
const ageRegex = /^(1[89]|[2-7][0-9]|80)$/;


const okButton = document.querySelector('.ok-btn');

okButton.addEventListener("click", function (event) {
    if (!cityRegex.test(cityInput.value)) {
    cityInput.setCustomValidity("Incorrect city name!");   
    }
    else{
    cityInput.setCustomValidity('');  
    }
    cityInput.reportValidity();
    if (!firstNameRegex.test(firstNameInput.value)) {
        firstNameInput.setCustomValidity("Incorrect first name!");
    }
    else {
        firstNameInput.setCustomValidity('');
    }
    firstNameInput.reportValidity();
    if (!lastNameRegex.test(lastNameInput.value)) {
        lastNameInput.setCustomValidity("Incorrect last name!");
    }
    else {
        lastNameInput.setCustomValidity('');
    }
    lastNameInput.reportValidity();
    let startDate = new Date(startRentInput.value);
    let endDate = new Date(endRentInput.value);
    if (startDate.getTime() > endDate.getTime()) {
        startRentInput.setCustomValidity("Start rent cant be later than end rent!");
    }
    else {
        startRentInput.setCustomValidity('');
    }
    startRentInput.reportValidity();
    if (!ageRegex.test(ageInput.value)) {
        ageInput.setCustomValidity("Age for auto rent should be from 18 to 80!");
    }
    else {
        ageInput.setCustomValidity('');
    }
    ageInput.reportValidity();
});

autoForm.addEventListener("submit",function(event){
event.preventDefault();
alert("Form is submitted!");    
});